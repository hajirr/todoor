import React, { useRef } from 'react'
import './styles.css'

//intinya menginisialisasi tipe data untuk parameter component InputField
interface Props {
    todo: string;
    setTodo: React.Dispatch<React.SetStateAction<string>>;
    handleAdd: (e: React.FormEvent) => void;
}
//component InputField mene
export const InputField: React.FC<Props> = ({ todo, setTodo, handleAdd }: Props) => {

    //react hooks menggunakan useRef
    const inputRef = useRef<HTMLInputElement>(null);
    return (
        <div>
            <form action="" className="input" onSubmit={(e) => {
                handleAdd(e);
                inputRef.current?.blur();
            }
            }>
                <input className='input__box'
                    ref={inputRef}
                    value={todo}
                    onChange={(e) => setTodo(e.target.value)}
                    type="input" placeholder='Enter a task' />
                <button className="input__submit" type='submit'>Go</button>
            </form>
        </div>
    )
}
