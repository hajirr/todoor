import React, { useEffect, useRef, useState } from 'react'
import { Todo } from '../model'
import { AiFillDelete, AiFillEdit } from 'react-icons/ai'
import { MdDone } from 'react-icons/md'
import './styles.css'
type Props = {
    todo: Todo,
    todos: Todo[],
    setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
};

export const SingleTodo = ({ todo, todos, setTodos }: Props) => {

    const [edit, setEdit] = useState<boolean>(false);
    const [editTodo, setEditTodo] = useState<string>(todo.todo);

    const handleDone = (id: number) => {
        // merubah nilai isDone menjadi true
        setTodos(todos.map((todo) => todo.id === id ? { ...todo, isDone: !todo.isDone } : todo))
    };

    const handleDelete = (id: number) => {
        // filter id 
        setTodos(todos.filter((todo) => todo.id !== id));
    };

    const handleEdit = (e: React.FormEvent, id: number) => {
        e.preventDefault();
        setTodos(todos.map((todo) => (
            todo.id === id ? { ...todo, todo: editTodo } : todo
        )))
        setEdit(false)
    }

    useEffect(() => {
        inputRef.current?.focus();
    }, [edit])

    const inputRef = useRef<HTMLInputElement>(null);

    return (
        <form className="todos__single" onSubmit={(e) => handleEdit(e, todo.id)}>
            {
                edit ? (
                    <input ref={inputRef} className='todos__single--text' type="" value={editTodo} onChange={(e) => setEditTodo(e.target.value)} />
                )
                    // jika todo isDone itu true
                    : todo.isDone ? (
                        // maka todo.todo dicoret
                        <s className="todos__single--text">{todo.todo}</s>
                    ) : (
                        <span className="todos__single--text">{todo.todo}</span>
                    )
            }
            <span className="icon" onClick={() => {
                if (!edit && !todo.isDone) {
                    setEdit(!edit)
                }
            }
            }>
                {/* icon dari react-icons */}
                <AiFillEdit />
            </span>
            <span className="icon" onClick={() => handleDelete(todo.id)}>
                {/* icon dari react-icons */}
                <AiFillDelete />
            </span>
            <span className="icon" onClick={() => handleDone(todo.id)}>
                {/* icon dari react-icons */}
                <MdDone />
            </span>
        </form>
    )
}
